const express = require('express');
const config = require('./config/config');
const bodyParser = require('body-parser');
var cors = require('cors')
const crypto  = require('crypto');


const app = express();
const PORT = process.env.PORT || 8000;

app.use(cors())


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


app.post('/get_signature', (req, res) => {
 
     
     const  bodyParser = req.body;
     const  {
         appId,
        customerEmail,
        customerName,
        customerPhone,
        notifyUrl,
        orderAmount,
        orderId,
        returnUrl,
    } =  req.body;
    
    console.log(req.body)
    var keys = Object.keys(bodyParser);
    keys.sort();
    signatureData = "";
    keys.forEach((key)=>{
    
        if(key !='signature'){
            signatureData += key+bodyParser[key];
        }
        
    });
    console.log(signatureData)
    var signature =  crypto.createHmac('sha256','c20109304b4e9a1c2d107dfda285cf84acf281ab').update(signatureData).digest('base64');

    res.send({status:'success',signature:signature})



})


app.get('/', (req, res) => {
    res.send("Wel come to cash fee integration")
})
app.post('/payment_result', (req, res) => {
     
    console.log(req.body,JSON.stringify(req.body))
    return res.redirect('https://cashfree-angular.surge.sh/#/blog/payment_success/data:'+JSON.stringify(req.body));


})

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`)
})
